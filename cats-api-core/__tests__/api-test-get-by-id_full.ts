import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

// const cats: CatMinInfo[] = [{ name: 'Суперавтотестеркрутыш', description: '', gender: 'male' }];

let catId: number;
const fakeId = 'fakeId';
const nonExistCatId = 999999;

const HttpClient = Client.getInstance();

describe('API Core - поиск котика', () => {

    beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: {
                    "cats": [
                        {
                            name: "Супертестировщиккотэ",
                            description: "Потрясный котэтестировщик",
                            gender: "male"
                        }
                    ]
                },
            });
            console.log(add_cat_response.body);

            if ((add_cat_response.body as CatsList).cats[0].id) {
                catId = (add_cat_response.body as CatsList).cats[0].id;
            } else throw new Error('Не удалось получить id тестового котика!');

        } catch (error) {
            console.error(error);  // Добавьте логирование самой ошибки
            throw new Error('Не удалось создать котика для автотестов!');
        }
    });

    afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
            responseType: 'json',
        });
    });

    it('Метод получения котика', async () => {
        try {
            const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
                responseType: 'json',
            });
            expect(response.statusCode).toEqual(200);

            expect(response.body).toEqual({
                cat: expect.objectContaining({
                    id: expect.any(Number),
                    name: expect.any(String),
                    description: expect.any(String),
                    gender: expect.any(String),
                    likes: expect.any(Number),
                    dislikes: expect.any(Number),
                }),
            });

        } catch (error) {
            console.error(error);  // Добавьте логирование самой ошибки
            throw new Error('Не удалось получить котика!');
        }
    });

    it('Метод получения котика с некорректным id', async () => {
        try {
            const response = await HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
                responseType: 'json',
            });
        } catch (error) {
            expect(error.response.statusCode).toEqual(400);
        }
    });

    it('Метод получения котика, если котик с переданным id не найден', async () => {
        try {
            const response = await HttpClient.get(`core/cats/get-by-id?id=${nonExistCatId}`, {
                responseType: 'json',
            });
        } catch (error) {
            expect(error.response.statusCode).toEqual(404);
        }
    });

});


