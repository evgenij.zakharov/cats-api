import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

// const cats: CatMinInfo[] = [{ name: 'Суперавтотестеркрутыш', description: '', gender: 'male' }];

let catId: number;
const fakeId = 'fakeId';
const nonExistCatId = 999999;
const limit = 1;
const order = 'desc';
const gender = "male";


const HttpClient = Client.getInstance();

describe('API Core - получение всех котиков', () => {

    it('Метод получения всех котиков с пустыми параметрами', async () => {
        try {
            const response = await HttpClient.get(`core/cats/allByLetter`, {
                responseType: 'json',
            });
            expect(response.statusCode).toEqual(200);

            expect(response.body).toEqual({
                groups: expect.arrayContaining([
                    expect.objectContaining({
                        cats: expect.arrayContaining([
                            expect.objectContaining({
                                    id: expect.any(Number),
                                    name: expect.any(String),
                                    description: expect.any(String),
                                    gender: expect.any(String),
                                    likes: expect.any(Number),
                                    dislikes: expect.any(Number),
                                }
                            )
                        ]),
                        title: expect.any(String),
                        count_in_group: expect.any(Number),
                        count_by_letter: expect.any(Number)
                    }),
                ]),
                count_output: expect.any(Number),
                count_all: expect.any(Number)
            });
        } catch (error) {
            throw new Error('Не удалось получить группу котиков!');
        }
    });


    it('Метод получения всех котиков с заполненными параметрами', async () => {
        try {
            const response = await HttpClient.get(`core/cats/allByLetter?limit=${limit}&order=${order}&gender=${gender}`, {
                responseType: 'json',
            });
            expect(response.statusCode).toEqual(200);

            expect(response.body).toEqual({
                groups: expect.arrayContaining([
                    expect.objectContaining({
                        cats: expect.arrayContaining([
                            expect.objectContaining({
                                    id: expect.any(Number),
                                    name: expect.any(String),
                                    description: expect.any(String),
                                    gender: gender,
                                    // gender: expect.any(String),
                                    likes: expect.any(Number),
                                    dislikes: expect.any(Number),
                                }
                            )
                        ]),
                        title: expect.any(String),
                        count_in_group: limit,
                        // count_in_group: expect.any(Number),
                        count_by_letter: expect.any(Number)
                    }),
                ]),
                count_output: expect.any(Number),
                count_all: expect.any(Number)
            });
        } catch (error) {
            throw new Error('Не удалось получить группу котиков!');
        }
    });

});


