import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

// const cats: CatMinInfo[] = [{ name: 'Суперавтотестеркрутыш', description: '', gender: 'male' }];

let catId: number;
const fakeId = 'fakeId';
const nonExistCatId = 999999;
const descriptionToAdd = "Малыш потрясен и великолепен! Статен, грациозен и силен!";

const HttpClient = Client.getInstance();

describe('API Core - добавление описания котика', () => {

    beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: {
                    "cats": [
                        {
                            name: "Супертестировщиккотэ",
                            description: "Потрясный котэтестировщик",
                            gender: "male"
                        }
                    ]
                },
            });
            console.log(add_cat_response.body);

            if ((add_cat_response.body as CatsList).cats[0].id) {
                catId = (add_cat_response.body as CatsList).cats[0].id;
            } else throw new Error('Не удалось получить id тестового котика!');

        } catch (error) {
            console.error(error);  // Добавьте логирование самой ошибки
            throw new Error('Не удалось создать котика для автотестов!');
        }
    });

    afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
            responseType: 'json',
        });
    });

    it('Метод сохранения описания котика', async () => {
        try {
            const response = await HttpClient.post('core/cats/save-description', {
                responseType: 'json',
                json: {
                    catId: catId,
                    catDescription: descriptionToAdd

                },
            });

            expect(response.statusCode).toEqual(200);

            expect(response.body).toMatchObject({
                id: catId,
                description: descriptionToAdd,
                gender: "male",
                likes: 0,
                dislikes: 0,
            });
        } catch (error) {
            console.error(error);  // Добавьте логирование самой ошибки
            throw new Error('Не удалось сохранить описание котика!');
        }
    });

    it('Метод сохранения описания котика с некорректным id ', async () => {
        try {
            const response = await HttpClient.post('core/cats/save-description', {
                responseType: 'json',
                json: {
                    catId: fakeId,
                    catDescription: descriptionToAdd
                },
            });
        } catch (error) {
            expect(error.response.statusCode).toEqual(400);
        }
    });


    it('Метод сохранения описания котика, если котика с переданным id не существует ', async () => {
        try {
            const response = await HttpClient.post('core/cats/save-description', {
                responseType: 'json',
                json: {
                    catId: nonExistCatId,
                    catDescription: descriptionToAdd
                },
            });
        } catch (error) {
            expect(error.response.statusCode).toEqual(404);
        }
    });

});


